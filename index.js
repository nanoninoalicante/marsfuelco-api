if (process.env.SERVER === "local") {
  require("dotenv").config({});
}
const PORT = process.env.PORT || 8888;
const express = require("express");
const app = express();
const cors = require("cors");
const got = require("got");
const axios = require("axios");
const Mongo = require("./classes/Mongo");
const mongo = new Mongo(process.env.MONGODB_HOST, process.env.MONGODB_DB_NAME);
const { resolve } = require("path");
// This is your real test secret API key.
const stripe = require("stripe")(process.env.STRIPE_SK_KEY);
app.use(express.static("."));
app.use(express.json());
app.use(cors());
const calculateOrderAmount = (items) => {
  // Replace this constant with a calculation of the order's amount
  // Calculate the order total on the server to prevent
  // people from directly manipulating the amount on the client
  return 1400;
};
app.post("/create-payment-intent", async (req, res) => {
  const { items = [] } = req.body;
  const { amount = 0 } = req.body;
  const { customerId = null } = req.body;

  // Create a PaymentIntent with the order amount and currency
  let params = {
    amount,
    currency: "eur",
  };
  if (customerId) {
    params.customer = customerId;
  }
  console.log("customer: ", customerId);
  console.log("params: ", params);

  const paymentIntent = await stripe.paymentIntents.create(params);
  res.send({
    clientSecret: paymentIntent.client_secret,
  });
});
app.post("/create-subscription", async (req, res) => {
  const { items = [] } = req.body;
  const { customerId = null } = req.body;
  const subscriptionPriceId = "price_1JDouYDDF4yhC66h0WThxUYh";
  // Create a PaymentIntent with the order amount and currency
  const params = {
    customer: customerId,
    items: [
      {
        price: subscriptionPriceId,
      },
    ],
    add_invoice_items: [
      {
        price: "price_1JDowJDDF4yhC66h2L6diTJj",
      },
    ],
    payment_behavior: "default_incomplete",
    expand: ["latest_invoice.payment_intent"],
  };
  console.log("customer: ", customerId);
  console.log("params: ", params);

  const subscription = await stripe.subscriptions.create(params);
  res.send({
    subscriptionId: subscription.id,
    clientSecret: subscription.latest_invoice?.payment_intent?.client_secret,
  });
});
app.post("/create-session", async (req, res) => {
  const { items = [] } = req.body;
  const { amount = 0 } = req.body;
  const { customerId = null } = req.body;

  // Create a PaymentIntent with the order amount and currency
  let params = {
    amount,
    currency: "eur",
  };
  if (customerId) {
    params.customer = customerId;
  }
  console.log("customer: ", customerId);
  console.log("params: ", params);
  const session = await stripe.checkout.sessions.create({
    payment_method_types: ["card"],
    line_items: [
      {
        price_data: {
          currency: "usd",
          product_data: {
            name: "Stubborn Attachments",
            images: ["https://i.imgur.com/EHyR2nP.png"],
          },
          unit_amount: 2000,
        },
        quantity: 1,
      },
    ],
    mode: "payment",
    success_url: `http://127.0.0.1:3000/?status=success`,
    cancel_url: `http://127.0.0.1:3000/?status=failed`,
  });
  res.send({
    url: session.url,
  });
});

app.get("/sheets", async (req, res) => {
  const sheetId = "1ezOmTF5ctKQ-skbETA3u0vv7eGtzhYKH9EqnyFjYK6o";
  const response = await got
    .get(
      `https://sheetsapi-dev.apis.gtrthelookout.com/sheets/${sheetId}`,
      {
        resolveBodyOnly: true,
        searchParams: {
          sheet_name: "October-2021",
          api_key: "76b940f2-4837-44d2-960a-6b1b491a2c59",
          formatted: true,
        },
      }
    )
    .catch((e) => {
      console.log("error: ", e.message);
      throw new Error(e.message);
    });

  res.status(200).json({
    testing: JSON.parse(response),
  });
});

app.get("/sheets2", async (req, res) => {
  const sheetId = "1ZSUuuv4WQ1zdbSxbgCpHwd1emWvhFYNHozSPH4Ga-X0";
  const response = await axios
    .get(
      `https://sheetsapi-dev.apis.gtrthelookout.com/sheets/${sheetId}`,
      {
        responseType: "json",
        params: {
          sheet_name: "January-2021",
          api_key: "76b940f2-4837-44d2-960a-6b1b491a2c59",
          formatted: true,
        },
      }
    )
    .catch((e) => {
      console.log("error: ", e.message);
      throw new Error(e.message);
    });

  console.log("response: ", response.data);

  res.status(200).json({
    response: response.data,
  });
});

app.get("/mongodb", (req, res) => {
  res.status(200).json({
    testing: "connection",
  });
});

app.get("/testip", async (req, res) => {
  const { body = {} } = await got.post(
    "https://webhook.site/dfe37ef6-563c-486a-910f-d3570020113d",
    {
      json: {
        hello: new Date().toISOString(),
      },
      responseType: "json",
    }
  );
  res.status(200).json({
    response: body,
  });
});

app.get("/users", async (req, res) => {
  await mongo.connect().catch((e) => {
    return res.status(400).json({
      error: e.message,
    });
  });
  const users = await mongo.find({ collection: "users", query: {} });

  res.status(200).json({
    response: users,
  });
});

app.listen(PORT, () => console.log(`Node server listening on port ${PORT}!`));
