import dotenv from "dotenv";
dotenv.config();
import * as admin from "firebase-admin";
admin.initializeApp({ credential: admin.credential.applicationDefault() });
import test from "ava";
const email = "cjameshill@me.com";

// test("get user by email", async (t) => {
//   await admin
//     .auth()
//     .getUserByEmail(email)
//     .then((userRecord) => {
//       // See the UserRecord reference doc for the contents of userRecord.
//       console.log(
//         `Successfully fetched user data: ${JSON.stringify(userRecord.toJSON())}`
//       );
//
//       t.pass();
//     })
//     .catch((error) => {
//       console.log("Error fetching user data:", error);
//       t.fail();
//     });
//   t.pass();
// });

test("get email verification", async (t) => {
  const r = await admin.auth().generateEmailVerificationLink(email);
  console.log("response: ", r);
  t.pass();
});

// test("create new user", async (t) => {
//     const r = await admin
//         .auth()
//         .createUser({
//             email: 'cjameshill@me.com',
//             emailVerified: false,
//         });
//   console.log("response: ", r);
//   t.pass();
// });
