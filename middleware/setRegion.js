
module.exports = () => (req, res, next) => {
    res.locals.region = req.params.region;
    next();
};
