"use strict";
const MongoClient = require("mongodb").MongoClient;

class Mongo {
  constructor(host, dbName, user = null, password = null) {
    this.host = host;
    this.dbName = dbName;
    this.user = user;
    this.password = password;
    this.db = null;
    this.client = null;
  }

  get getClient() {
    return this.client;
  }

  closeClient() {
    if (!this.client) return null;
    console.log("Closing Mongo Client");
    return this.client.close();
  }

  connect() {
    console.log("host: ", this.host);
    console.log("db: ", this.db);
    console.log("client: ", this.client);

    if (this.client) {
      console.log("Mongo Client is already set");
      return Promise.resolve({db: this.db, client: this.client});
    }

    let options = {
      useNewUrlParser: true
    };
    if (this.user) {
      options.auth = {
        user: this.user,
        password: this.password
      };
    }

    return MongoClient.connect(this.host, options).then(client => {
      console.log("Connected successfully to server");
      this.db = client.db(this.dbName);
      this.client = client;
      return {db: this.db, client};
    });
  }

  insert({collection, db = null, records}) {
    if (!this.db) return Promise.reject("Missing db connection");
    const coll = this.db.collection(collection);
    return new Promise((resolve, reject) => {
      coll.insertMany(records, (err, response) => {
        if (err) {
          console.log("Error from Mongo: ", err.message);
          if (err.message.includes("duplicate key error")) {
            return resolve(err.message);
          }
          return reject(err.message);
        }
        return resolve(response);
      });
    });
  }

  find({collection, db = null, query = {}, projection = {}}) {
    if (!this.db) return Promise.reject("Missing db connection");
    const coll = this.db.collection(collection);
    return new Promise((resolve, reject) => {
      coll
          .find(query, {projection})
          .sort({_id: 1})
          .toArray((err, response) => {
            if (err) {
              return reject(err);
            }
            return resolve(response);
          });
    });
  }
}
module.exports = Mongo;
