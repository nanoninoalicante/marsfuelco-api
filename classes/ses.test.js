require("dotenv").config();
const test = require("ava");
const aws = require("aws-sdk");
const ses = new aws.SESV2({
  region: "us-east-2",
  accessKeyId: process.env.ACCESS_KEY_ID,
  secretAccessKey: process.env.SECRET_ACCESS_KEY,
});

test("send ses email", async (t) => {
  const params = {
    Content: {
      Simple: {
        Body: {
          Html: {
            Data: `hello testing - ${new Date().toISOString()}`,
          },
        },
        Subject: {
          Data: "Testing SES V2",
        },
      },
    },
    Destination: {
      ToAddresses: ["cjameshill@gmail.com"],
    },
    FromEmailAddress: "Mars Fuel Co <christopher@nanonino.com>",
    FromEmailAddressIdentityArn:
      "arn:aws:ses:us-east-2:912068501224:identity/christopher@nanonino.com",
  };
  await ses.sendEmail(params).promise();
  t.pass();
});
