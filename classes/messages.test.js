require("dotenv").config();
const test = require("ava");
const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const client = require('twilio')(accountSid, authToken);

test("send sms", async (t) => {
  await client.messages
      .create({
        body: 'This is the ship that made the Kessel Run in fourteen parsecs?',
        from: '+447412452754',
        to: '+34644632342'
      })
      .then(message => console.log(message.sid));
  t.pass();
});
//
// test("send whatsapp", async (t) => {
//   await client.messages
//       .create({
//         body: 'This is the ship that made the Kessel Run in fourteen parsecs?',
//         from: 'whatsapp:+14155238886',
//         to: 'whatsapp:+447411103268'
//       })
//       .then(message => console.log(message.sid));
//   t.pass();
// });
